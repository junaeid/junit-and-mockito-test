package com.junitandmockitotest.app.repositories;

import com.junitandmockitotest.app.models.Employee;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

@DataJpaTest
public class EmployeeRepositoryTests {

    @Autowired
    private EmployeeRepository employeeRepository;

    private Employee employee;

    @BeforeEach
    public void setup() {
        employee = Employee.builder()
                .firstName("juni")
                .lastName("ahmed")
                .email("email@gmail.com")
                .build();
    }

    // Junit test for save
    @DisplayName("Junit test for save employee operation")
    @Test
    public void givenEmployeeObject_whenSave_thenReturnSavedEmployeeObject() {
        // given - precondition or setup


        // when - action or behavior that we are going to test
        Employee savedEmployee = employeeRepository.save(employee);

        // then - verify the output
        assertThat(savedEmployee).isNotNull();
        assertThat(savedEmployee.getId()).isGreaterThan(0);

    }

    @DisplayName("Junit test for get all employees operation")
    @Test
    public void givenEmployeeList_whenFindAll_thenEmployeeList() {
        // given - precondition or setup
        Employee employee2 = Employee.builder()
                .firstName("junaeid")
                .lastName("ahmed")
                .email("junaeid@gmail.com")
                .build();

        employeeRepository.save(employee);
        employeeRepository.save(employee2);

        // when - action or behavior that we are going to test
        List<Employee> employeeList = employeeRepository.findAll();

        // then - verify the output
        assertThat(employeeList).isNotNull();
        assertThat(employeeList.size()).isEqualTo(2);
    }

    @DisplayName("Junit test for get employee by Id Operation")
    @Test
    public void givenEmployeeObject_whenFindById_thenReturnEmployeeById() {
        // given - precondition or setup
        employeeRepository.save(employee);

        // when - action or behavior that we are going to test
        Employee savedEmployee = employeeRepository.findById(employee.getId()).orElse(null);

        // then - verify the output

        assertThat(savedEmployee).isNotNull();
        assertThat(savedEmployee.getId()).isEqualTo(employee.getId());
    }

    @DisplayName("Junit test for get employee by email operation")
    @Test
    public void givenEmployeeEmail_whenFindByEmail_thenEmployeeObject() {
        // given - precondition or setup
        employeeRepository.save(employee);
        // when - action or behavior that we are going to test
        Employee savedEmployee = employeeRepository.findByEmail(employee.getEmail()).orElse(null);

        // then - verify the output
        assertThat(savedEmployee).isNotNull();
        assertThat(savedEmployee.getEmail()).isEqualTo(employee.getEmail());
    }


    @DisplayName("Junit test for update employee operation")
    @Test
    public void givenEmployeeObject_whenUpdateById_thenReturnUpdatedEmployee() {
        // given - precondition or setup
        employeeRepository.save(employee);

        // when - action or behavior that we are going to test
        Employee savedEmployee = employeeRepository.findById(employee.getId()).orElse(null);
        assert savedEmployee != null;
        savedEmployee.setEmail("custom@gmail.com");
        savedEmployee.setFirstName("updated");
        savedEmployee.setLastName("name");
        Employee updatedEmployee = employeeRepository.save(savedEmployee);


        // then - verify the output
        assertThat(updatedEmployee.getEmail()).isEqualTo("custom@gmail.com");
        assertThat(updatedEmployee.getFirstName()).isEqualTo("updated");
        assertThat(updatedEmployee.getLastName()).isEqualTo("name");


    }

    @DisplayName("Junit test for delete employee operation ")
    @Test
    public void givenEmployeeObject_whenDelete_thenRemoveEmployee() {
        // given - precondition or setup
        employeeRepository.save(employee);

        // when - action or behavior that we are going to test
        employeeRepository.delete(employee);
        Optional<Employee> employeeOptional = employeeRepository.findById(employee.getId());

        // then - verify the output
        assertThat(employeeOptional).isEmpty();
    }


    @DisplayName("Junit test for custom query using jpql with index ")
    @Test
    public void givenFirstNameAndLastName_whenFindByJPQL_thenReturnEmployeeObject() {
        // given - precondition or setup
        employeeRepository.save(employee);

        String firstName = "juni";
        String lastName = "ahmed";

        // when - action or behavior that we are going to test
        Employee savedEmployee = employeeRepository.findByJPQL(firstName, lastName);

        // then - verify the output
        assertThat(savedEmployee).isNotNull();
    }

    @DisplayName("Junit test for custom query using jpql with named ")
    @Test
    public void givenFirstNameAndLastName_whenFindByJPQLNamedParams_thenReturnEmployeeObject() {
        // given - precondition or setup
        employeeRepository.save(employee);

        String firstName = "juni";
        String lastName = "ahmed";

        // when - action or behavior that we are going to test
        Employee savedEmployee = employeeRepository.findByJPQLNamedParams(firstName, lastName);

        // then - verify the output
        assertThat(savedEmployee).isNotNull();
    }


}
